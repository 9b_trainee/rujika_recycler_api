package com.internship.api_recycler_view.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetDataSources {
    @Expose
    @SerializedName("tests")
    private List<Tests> tests;
    @Expose
    @SerializedName("success")
    private boolean success;

    public List<Tests> getTests() {
        return tests;
    }

    public void setTests(List<Tests> tests) {
        this.tests = tests;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
