package com.internship.api_recycler_view.Interface;

import com.internship.api_recycler_view.Model.GetDataSources;
import com.internship.api_recycler_view.Model.Tests;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface DataSuccess {

    @GET("v1/tests")
    Call<GetDataSources> getAllPost();
}
