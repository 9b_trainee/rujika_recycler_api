package com.internship.api_recycler_view;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.internship.api_recycler_view.Adapter.NewAdapter;
import com.internship.api_recycler_view.Adapter.Test;
import com.internship.api_recycler_view.Constant.BaseUrl;
import com.internship.api_recycler_view.Interface.DataSuccess;
import com.internship.api_recycler_view.Model.GetDataSources;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RecyclerViewApiDemo extends AppCompatActivity {

    private final String TAG = "RecyclerVIewAPi";
    private RecyclerView recyclerView;
    String id,name,message;
    private List<Test> testList = new ArrayList<>();
    private NewAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view_api);
        mAdapter = new NewAdapter(testList);
        recyclerView = findViewById(R.id.recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(mAdapter);

        AsyncTaskRunner runner = new AsyncTaskRunner();
        runner.execute();

    }


    private class AsyncTaskRunner extends AsyncTask<String, String, String> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // display a progress dialog for good user experiance
            progressDialog = new ProgressDialog(RecyclerViewApiDemo.this);
            progressDialog.setMessage("Please Wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }



        @Override
        protected String doInBackground(String... strings) {

            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(100, TimeUnit.SECONDS)
                    .readTimeout(100, TimeUnit.SECONDS).build();
            Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(BaseUrl.baseUrl).addConverterFactory(GsonConverterFactory.create(gson)).build();
            final DataSuccess requestInterface = retrofit.create(DataSuccess.class);
            final Call<GetDataSources> GetDataSourcesCall = requestInterface.getAllPost();

            GetDataSourcesCall.enqueue(new Callback<GetDataSources>() {
                @Override
                public void onResponse(Call<GetDataSources> call, Response<GetDataSources> response) {
                    progressDialog.dismiss();
                    Log.e("asyn task","Working Background");
                    try
                    {
                        if(response.isSuccessful()){
                            if(response.body().getSuccess()){
                                for(int i=0;i<response.body().getTests().size();i++)
                                {
                                    id= String.valueOf(response.body().getTests().get(i).getId());
                                    name=response.body().getTests().get(i).getName();
                                    message=response.body().getTests().get(i).getMessage1();
                                    Test information=new Test(name,id,message);
                                    testList.add(information);
                                }
                                mAdapter.notifyDataSetChanged();

                            }
                            else {
                                Toast.makeText(RecyclerViewApiDemo.this, "api problem", Toast.LENGTH_SHORT).show();
                            }
                        }
                        else {
                            Toast.makeText(RecyclerViewApiDemo.this, "try again", Toast.LENGTH_SHORT).show();
                        }
                    }

                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    finally {
                        Log.e("Not Working","Problem in background");
                    }
                }


                @Override
                public void onFailure(Call<GetDataSources> call, Throwable t) {
                    Log.d(TAG, "error loading from API");
                    Log.e("Fail","Error loading from API");
                    progressDialog.dismiss();
                }

            });

            return null;
        }
    }
}
