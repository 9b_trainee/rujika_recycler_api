package com.internship.api_recycler_view.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.internship.api_recycler_view.Model.GetDataSources;
import com.internship.api_recycler_view.Model.Tests;
import com.internship.api_recycler_view.R;

import java.util.List;

public class NewAdapter extends RecyclerView.Adapter<NewAdapter.MyViewHolder> {


    private List<Test> testList;

public class MyViewHolder extends RecyclerView.ViewHolder {
    public TextView title, id, message;

    public MyViewHolder(View view) {
        super(view);
        title = (TextView) view.findViewById(R.id.name);
        id = (TextView) view.findViewById(R.id.id);
        message = (TextView) view.findViewById(R.id.message);
    }
}

    public NewAdapter(List<Test> testList) {
        this.testList = testList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Test test = testList.get(position);
        holder.title.setText(test.getName());
        holder.id.setText(test.getId());
        holder.message.setText(test.getMessage());
    }

    @Override
    public int getItemCount() {
        return testList.size();
    }

}