package com.internship.api_recycler_view.Adapter;

public class Test {

    String name, id, message;

    public Test() {
    }

    public Test(String name, String id, String message) {
        this.name = name;
        this.id = id;
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
